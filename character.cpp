#include "character.h"


character_t::character_t(){
  setTime();
  _isAlive = true;
}

character_t::~character_t(){

  
}

int character_t::parseColor(std::string c){
    _color = color2index(c);
  return _color;
}
int character_t::parseAbil(std::string a){
  return ability2index(a);
}

int character_t::parseSpeed(std::string s){
  _speedDice = dice(s);
  return _speedDice.roll();
}

int character_t::parseHP(std::string h){
  _hpDice = dice(h);
return _hpDice.roll();  
}
dice character_t::parseDam(std::string d){
 return dice(d); 
}

bool character_t::operator()(character_t* a,character_t* b){
      
  
	if((*a).eventTime==(*b).eventTime){
	   return (*a).getTime()<(*b).getTime();
	}
	
	return (*a).eventTime<(*b).eventTime;
   }

void character_t::setSym(char c){
  sym = c;
}

char character_t::getSym(){
   return sym;
}

void character_t::setTime(){
  startTime = std::chrono::system_clock::now();
}

void character_t::setEventTime(){
  speed = speed == 0? 1:speed;
  eventTime = 1000/speed;
  setTime();
}

void character_t::setEventTime(int e){
  eventTime = e;
  setTime();
}



int character_t::getEventTime(){
  return eventTime;
}
int character_t::getSpeed(){
  return speed;
}

void character_t::decreaseEventTime(int i){
  eventTime-=i;
  eventTime = eventTime >0?eventTime:0;
}


unsigned int character_t::getTime(){
  return std::chrono::duration_cast<std::chrono::milliseconds>
  (std::chrono::system_clock::now() - startTime).count();
}

void character_t::setPos(position_t p){
  pos = p;
  npos = p;
}
position_t character_t::getPos(){
  return pos;
}

void character_t::setAlive(bool a){
_isAlive = a;
}

bool character_t::getAlive(){
return _isAlive;
}

void character_t::renderCharacter(){
  WINDOW* gameWind = console->getGameWindow();
  if(_isAlive)
  mvwaddch(gameWind,pos.y(),pos.x(),getSym());
}
