#ifndef CHARACTER_H
#define CHARACTER_H

#include <chrono>
#include "console.h"
#include "util.h"


class character_t{
  public:
  character_t();
  virtual ~character_t();
  
  void setTime();
  unsigned int getTime();
  position_t getPos();
  void renderCharacter();
  void setSym(char c);
  char getSym();
  void setEventTime();
  void setEventTime(int e);
  int getEventTime();
  void setPos(position_t p);
  bool operator()(character_t* a,character_t* b);
  void setAlive(bool a);
  bool getAlive();
  
  void decreaseEventTime(int i);
  int getSpeed(); 
  
  int parseColor(std::string c);
  int parseAbil(std::string a);
  int parseHP(std::string h);
  dice parseDam(std::string d);
  int parseSpeed(std::string s);
  
  std::string _name;
  std::string _desc;
  int _color;
  
  dice _hpDice;
  dice _dam;
  dice _speedDice;
  int _hp;
  int attr;
  
  position_t npos;
  std::chrono::time_point<std::chrono::system_clock> startTime;
  console_t* console;
  char sym;
  position_t pos;
  int speed;
  int eventTime;
  int _sight;
  bool _isAlive;
  
  
  
  private:

  protected:

  
  
};




#endif