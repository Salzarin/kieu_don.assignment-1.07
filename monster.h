#ifndef MONSTER_H
#define MONSTER_H

#include "character.h"
#include "player.h"
#include "util.h"
#include<vector>
#include <ncurses.h>

class monster_t : public character_t{
public:
  monster_t();
  ~monster_t();
  monster_t(console_t* con,player_t* p,  Dungeon * dungeon, int spd);
  monster_t(std::string name,std::string desc,std::string symbol,std::string color, std::string s, std::string abil,std::string hp,std::string dam);
  bool initMonsterPosition(bool mMap[MAX_V][MAX_H]);
  void initMonsterAttr();
  void setRandomPos();
  player_t * player;
  position_t dest;
  void monsterNextMove();
  void moveMonster();
  bool checkLos();
  void parseMonster();

  
  
  
  bool seen;
private:
  Dungeon* d;
  
};



#endif