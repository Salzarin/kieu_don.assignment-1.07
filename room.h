#ifndef ROOM_H
#define ROOM_H

#include "util.h"


class Room{
  public:
    Room();
    ~Room();
    Room(int x, int y, int w, int h);
    position_t pos;
    dimension_t dim;
    position_t cpos;
    bool valid;
    bool getValid();
    void setValid(bool set);
    int x();
    int y();
    int cx();
    int cy();
    int right();
    int bottom();
    int w();
    int h();
    bool checkRoomIntersection(Room B);
    position_t getRandomPosition();
    
private:

protected:  
  
  
  
};







#endif